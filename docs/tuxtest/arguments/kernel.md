# Kernel

`kernel` is a required argument, and should be a url to the kernel image to test.

This url should be publicly accessible like a tuxbuild url.

## Compression

It's possible to provide a `gz` compressed kernel image as is.
